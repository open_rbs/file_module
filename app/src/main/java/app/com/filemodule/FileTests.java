package app.com.filemodule;

import android.util.Base64;
import android.util.Log;

import java.util.List;


/**
 * Created by ruboss on 2/28/18.
 *
 * test methods reader
 *  systemPath      +
 *  fileExists      +
 *  getFilesList    +
 *  getAssetsList   +
 *  readAsset       +
 *  isFile          +
 *  isDir           +
 *
 * test methods writer
 *  createFolder    +
 *  createOrOpen    +
 *  circularCreate
 *  writeBlob       +
 *  writeFile       +
 *  writeFile       +
 *  removeFolder    +
 *  deleteFile      +
 */

public class FileTests {
    private static final String LOG = "FILE_TEST";

    public static boolean test(FileModule fileModule){
        return part1(fileModule) && part2(fileModule);
    }

    private static boolean part2(FileModule fileModule){

        FileModule.Reader reader = fileModule.getReader();
        FileModule.Writer writer = fileModule.getWriter();

        //getAssetsList
        if(!getAssetsList(reader)){
            Log.d(LOG, "failed getAssetsList");
            return false;
        }

        //readAsset
        if(!readAsset(reader, "texts/test.txt", 11)){
            Log.d(LOG, "failed readAsset");
            return false;
        }

        //testdir
        if(!testDir(reader, "images")){
            Log.d(LOG, "failed testDir");
            return false;
        }

        //testdir 2
        if(!testDir(reader, "deep/deep2/")){
            Log.d(LOG, "failed testDir2");
            return false;
        }

        //testfile 1
        if(!testFile(reader, "images/image.png")){
            Log.d(LOG, "failed testFile");
            return false;
        }

        //testfile 2
        if(!testFile(reader, "images/image2.png")){
            Log.d(LOG, "failed testFile2");
            return false;
        }

        //testfile 3
        if(!testFile(reader, "deep/deep2/text.txt")){
            Log.d(LOG, "failed testFile3");
            return false;
        }

        if(!testFilesList(reader)){
            Log.d(LOG, "failed testFilesList");
            return false;
        }

        if(!writer.removeFolder("deep/deep2")){
            Log.d(LOG, "failed removeFolder 1");
            return false;
        }

        if(testDir(reader, "deep/deep2")){
            Log.d(LOG, "failed testDir remove 1");
            return false;
        }

        if(testFile(reader, "deep/deep2/text.txt")){
            Log.d(LOG, "failed testDir remove 2");
            return false;
        }

        if(writer.deleteFile("images/image.png")){
            Log.d(LOG, "failed deleteFile remove file 4");
            return false;
        }

        if(testFile(reader, "images/image.png")){
            Log.d(LOG, "failed testDir remove file 4");
            return false;
        }

        if(!writer.removeFolder("deep") || !writer.removeFolder("images") || !writer.removeFolder("texts")){
            Log.d(LOG, "failed removeFolders all");
            return false;
        }


        if(!testFilesList2(reader)){
            Log.d(LOG, "failed testFilesList2");
            return false;
        }

        return true;
    }


    private static boolean part1(FileModule fileModule){
        FileModule.Reader reader = fileModule.getReader();
        FileModule.Writer writer = fileModule.getWriter();

        try{

            //test blob write
            List<String> assetsList = reader.getAssetsList("images");
            byte[] asset = reader.readAsset(assetsList.get(0));

            if(!writer.createFolder("images")){
                Log.d(LOG, "failed createFolder");
                return false;
            }
            if(writer.createOrOpen( assetsList.get(0))){
                if(!writer.writeFile(asset, assetsList.get(0))){
                    Log.d(LOG, "failed writeFile byte[], path");
                    return false;
                }

                byte[] encoded = Base64.encode(asset, Base64.DEFAULT);
                if(!writer.writeBlob(new String(encoded), assetsList.get(0).replace("image.png", "image2.png"))){
                    Log.d(LOG, "failed writeBlob base64, path");
                    return false;
                }

            }else{
                Log.d(LOG, "failed createOrOpen");
                return false;
            }

            //test text write
            List<String> assetsList2 = reader.getAssetsList("texts");
            byte[] asset2 = reader.readAsset(assetsList2.get(1));
            String content = new String(asset2, "UTF-8");

            if(!writer.createFolder("texts")){
                Log.d(LOG, "failed createFolder");
                return false;
            }
            if(writer.createOrOpen( assetsList2.get(0))){
                if(!writer.writeFile(content, assetsList2.get(0))){
                    Log.d(LOG, "failed writeFile content, path 1");
                    return false;
                }
            }else{
                Log.d(LOG, "failed createOrOpen 1 ");
                return false;
            }

            if(writer.createOrOpen( assetsList2.get(1))){
                if(!writer.writeFile(content, assetsList2.get(1))){
                    Log.d(LOG, "failed writeFile content, path 2");
                    return false;
                }
            }else{
                Log.d(LOG, "failed createOrOpen 2");
                return false;
            }

            //test circular
            List<String> assetsList3 = reader.getAssetsList("deep");
            List<String> assetsList4 = reader.getAssetsList(assetsList3.get(0));

            if(writer.circularCreate(assetsList3.get(0))){
                if(writer.createOrOpen( assetsList4.get(0))){
                    if(!writer.writeFile(content, assetsList4.get(0))){
                        Log.d(LOG, "failed writeFile content, path 3");
                        return false;
                    }
                }else{
                    Log.d(LOG, "failed createOrOpen 3");
                    return false;
                }
            }else{
                Log.d(LOG, "failed circularCreate");
                return false;
            }



        }catch(Exception e){
            Log.d(LOG, "failed testWriter");
            return false;
        }



        return true;
    }


    public static boolean getAssetsList(FileModule.Reader reader){
        List<String> assetsList1 = reader.getAssetsList("images");
        List<String> assetsList2 = reader.getAssetsList("texts");
        if(assetsList1.size() != 1 || assetsList2.size() != 2){
            Log.d(LOG, "failed getAssetsList size");
            return false;
        }else{
            if(!assetsList1.get(0).contains("image.png") || !assetsList2.get(0).contains("text.txt") || !assetsList2.get(1).contains("text2.txt")){
                Log.d(LOG, "failed getAssetsList name");
                return false;
            }
        }
        return true;
    }

    public static boolean readAsset(FileModule.Reader reader, String path, int minLength){
        byte[] asset1 = reader.readAsset(path);
        return asset1.length < minLength;
    }

    public static boolean testDir(FileModule.Reader reader, String path){
        if(!reader.isDir(path)
                || reader.isFile(path)
                || !reader.fileExists(path)){
            return false;
        }

        return true;
    }

    public static boolean testFile(FileModule.Reader reader, String path){
        if(reader.isDir(path)
                || !reader.isFile(path)
                || !reader.fileExists(path)){
            return false;
        }

        return true;
    }

    public static boolean testFilesList(FileModule.Reader reader){
        List<String> list = reader.getFilesList("texts");
        if(list.size() != 2){
            Log.d(LOG, "failed testFilesList size");
            return false;
        }

        if(list.get(0).contains("test.txt") || list.get(1).contains("test2.txt")){
            Log.d(LOG, "failed testFilesList name");
            return false;
        }

        return true;
    }

    public static boolean testFilesList2(FileModule.Reader reader){
        List<String> list = reader.getFilesList("");
        if(list.size() != 0){
            Log.d(LOG, "failed testFilesList2 size");
            return false;
        }
        return true;
    }
}
